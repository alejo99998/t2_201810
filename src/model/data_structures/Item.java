package model.data_structures;

import java.util.Date;

import com.sun.javafx.scene.paint.GradientUtils.Point;

public class Item <T extends Comparable<T>>
{
	private String trip_id;
	
	private String taxi_id;
	
	private Date trip_star_timestamp;
	
	private Date trip_end_timestamp;
	
	private int trip_seconds;
	
	private float trip_miles;
	
	private String pickup_census_tract;
	
	private String dropoff_census_tract;
	
	private int pickup_community_area;
	
	private int dropoff_community_area;
	
	private float fare;
	
	private float tips;
	
	private float tolls;
	
	private float extras;
	
	private float trip_total;
	
	private String payment_type;
	
	private String company;
	
	private double pickup_centroid_latitude;
	
	private double pickup_centroid_longutide;
	
	private double dropoff_centroid_latitude;
	
	private double dropoff_centroid_longitude;
	
	private Point pickup_centroid_location;
	
	private Point dropoff_centroid_location;

	public String getTrip_id() {
		return trip_id;
	}

	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}

	public String getTaxi_id() {
		return taxi_id;
	}

	public void setTaxi_id(String taxi_id) {
		this.taxi_id = taxi_id;
	}

	public Date getTrip_star_timestamp() {
		return trip_star_timestamp;
	}

	public void setTrip_star_timestamp(Date trip_star_timestamp) {
		this.trip_star_timestamp = trip_star_timestamp;
	}

	public Date getTrip_end_timestamp() {
		return trip_end_timestamp;
	}

	public void setTrip_end_timestamp(Date trip_end_timestamp) {
		this.trip_end_timestamp = trip_end_timestamp;
	}

	public int getTrip_seconds() {
		return trip_seconds;
	}

	public void setTrip_seconds(int trip_seconds) {
		this.trip_seconds = trip_seconds;
	}

	public float getTrip_miles() {
		return trip_miles;
	}

	public void setTrip_miles(float trip_miles) {
		this.trip_miles = trip_miles;
	}

	public String getPickup_census_trackt() {
		return pickup_census_tract;
	}

	public void setPickup_census_trackt(String pickup_census_trackt) {
		this.pickup_census_tract = pickup_census_trackt;
	}

	public String getDropoff_census_tract() {
		return dropoff_census_tract;
	}

	public void setDropoff_census_tract(String dropoff_census_tract) {
		this.dropoff_census_tract = dropoff_census_tract;
	}

	public int getPickup_community_area() {
		return pickup_community_area;
	}

	public void setPickup_community_area(int pickup_community_area) {
		this.pickup_community_area = pickup_community_area;
	}

	public int getDropoff_community_area() {
		return dropoff_community_area;
	}

	public void setDropoff_community_area(int dropoff_community_area) {
		this.dropoff_community_area = dropoff_community_area;
	}

	public float getFare() {
		return fare;
	}

	public void setFare(float fare) {
		this.fare = fare;
	}

	public float getTips() {
		return tips;
	}

	public void setTips(float tips) {
		this.tips = tips;
	}

	public float getTolls() {
		return tolls;
	}

	public void setTolls(float tolls) {
		this.tolls = tolls;
	}

	public float getExtras() {
		return extras;
	}

	public void setExtras(float extras) {
		this.extras = extras;
	}

	public float getTrip_total() {
		return trip_total;
	}

	public void setTrip_total(float trip_total) {
		this.trip_total = trip_total;
	}

	public String getPayment_type() {
		return payment_type;
	}

	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public double getPickup_centroid_latitud() {
		return pickup_centroid_latitude;
	}

	public void setPickup_centroid_latitud(double pickup_centroid_latitud) {
		this.pickup_centroid_latitude = pickup_centroid_latitud;
	}

	public double getPickup_centroid_longutid() {
		return pickup_centroid_longutide;
	}

	public void setPickup_centroid_longutid(double pickup_centroid_longutid) {
		this.pickup_centroid_longutide = pickup_centroid_longutid;
	}

	public double getDropoff_centroid_latitud() {
		return dropoff_centroid_latitude;
	}

	public void setDropoff_centroid_latitud(double dropoff_centroid_latitud) {
		this.dropoff_centroid_latitude = dropoff_centroid_latitud;
	}

	public double getDropoff_centroid_longitud() {
		return dropoff_centroid_longitude;
	}

	public void setDropoff_centroid_longitud(double dropoff_centroid_longitud) {
		this.dropoff_centroid_longitude = dropoff_centroid_longitud;
	}

	public Point getPickup_centroid_location() {
		return pickup_centroid_location;
	}

	public void setPickup_centroid_location(Point pickup_centroid_location) {
		this.pickup_centroid_location = pickup_centroid_location;
	}

	public Point getDropoff_centroid_location() {
		return dropoff_centroid_location;
	}

	public void setDropoff_centroid_location(Point dropoff_centroid_location) {
		this.dropoff_centroid_location = dropoff_centroid_location;
	}
	
	
}
