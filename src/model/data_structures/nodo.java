package model.data_structures;

public class nodo <T> 
{
	private nodo<T> siguiente;
	private T item;
	

	public nodo(T item2) 
	{
		siguiente=null;
		this.item=item2;
	}
	public void agregarSigu(nodo<T> object)
	{
		siguiente=(nodo<T>)object;
	}
	public nodo darSigui()
	{
		return siguiente;
	}
	public void setItem(T pItem)
	{
		item=pItem;
	}
	public T darItem()
	{
		return item;
	}
}
